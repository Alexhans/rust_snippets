#![allow(dead_code)]
// #![allow(unused_imports)]
use std::collections::HashMap; 
// use std::fmt;
// use std::cmp::Ordering;

/*
 * beefcabbdedcfaccabbe
 *
 * b: 5
 * c: 4
 * e: 4
 * a: 3
 * d: 2
 * f: 2
 */

/*
 * 1) List [b, e, e, ...] -> fn get_freq -> HashMap
 * 2) HashMap -> fn sort_to_vec -> Vec
 * 3) List -> fn (uses 2) -> output 
 */

fn split_string_into_vec(input: String) -> Vec<String> {
    let mut output: Vec<String> = Vec::new();
    for x in input.split("") {
        if x.len() > 0 {
            output.push(x.to_string());
        }
    }
    output
}

fn get_freq(input: Vec<String>) -> HashMap<String, i32> {
    let mut data: HashMap<String, i32> = HashMap::new();
    for x in input.iter().cloned() {
        let counter = data.entry(x).or_insert(0);
        *counter += 1;
    }
    data
}

// fn get_freq_node(input: vec<String) -> Node<'a> {
//     let mut data = Node<'a>::new();
// }

struct Tree<'a> {
    root: Option<Box<Node<'a>>>,
    size: i32,
}

impl<'a> Tree<'a> {
    pub fn new() -> Tree<'a> {
        Tree { size: 0, root: None }
    }
    pub fn put(&mut self, new_key: &'a str, new_freq: i32) {
        match self.root {
            Some(ref mut algo) => algo.put(new_key, new_freq),
            None => {
                let new_node = Node { key: new_key, left: None, right: None, freq: new_freq};
                let boxed_node = Some(Box::new(new_node));
                self.root = boxed_node;
            }
        }
    }
    pub fn get(&self, key: &'a str) -> Option<i32> {
        match self.root {
            Some(ref algo) => algo.get(key),
            None => None
        }
    }
}
struct Node<'a> {
    freq: i32,
    key: &'a str,
    left: Option<Box<Node<'a>>>,
    right: Option<Box<Node<'a>>>,
}

impl<'a> Node<'a> {
    // Hace las veces de NewDicc
    pub fn new(first_key: &'a str, first_freq: i32) -> Node<'a> {
        Node { key: first_key, left:None, right: None, freq: first_freq}
    }
    // Put
    pub fn put(&mut self, new_key: &'a str, new_freq: i32) {
        if self.key == new_key {
            return 
        }
        let target_node = if new_key < self.key { &mut self.left } else { &mut self.right };
        match target_node {
            &mut Some(ref mut subnode) => subnode.put(new_key, new_freq),   
            &mut None => {
                let new_node = Node { key: new_key, left: None, right: None, freq: new_freq};
                let boxed_node = Some(Box::new(new_node));
                *target_node = boxed_node;
            }
        }
    }

    pub fn get(&self, key: &'a str) -> Option<i32> {
        if self.key == key {
            return Some(self.freq);
        }
        let mut val: Option<i32> = None;
        if self.key > key {
            if let Some(ref option) = self.left {
                val = option.get(key);
            }
        } 
        else {
            if let Some(ref option) = self.right {
                val = option.get(key);
            }

        }
        val
    }

    pub fn domain(&self) {
        
    }

    pub fn show(&self) {
        println!("{}:{}", self.key, self.freq);
        if let Some(ref option) = self.left { println!("Left:"); option.show() }
        if let Some(ref option) = self.right { println!("Right:"); option.show() }
    }
}

fn sort_to_vec(input: HashMap<String, i32>) -> Vec<String> {
    let mut output = vec![];
    let mut intermediate = vec![];
    for (key, value) in input.iter() {
        intermediate.push((value, key));
    }
    for x in intermediate.iter() {
        output.push(format!("{}: {}", -x.0, x.1));
    }
    println!("{:?}", output);
    output.sort();
    // output.sort_by(|a, b| 
    //        if(a.0 > b.0) {return Ordering::Less; }
    //        else if (a.0 == b.0) {
    //             if (a.1 > b.1) { return Ordering::Greater; }
    //             return Ordering::Less;
    //        }
    //        else {return Ordering::Greater; }
    //        // b.cmp(a)
    //    );
    println!("{:?}", output);
    output
    // output
}

fn show_freq(input: Vec<String>) {
    println!("START Showing frequency");
    for x in input.iter() {
        println!("{}", x);
    }
    println!("END Showing frequency");
}

#[test] 
fn test_splitting_string_into_vec() {
    assert_eq!(split_string_into_vec("abc".to_string()), 
                vec!["a".to_string(), "b".to_string(), "c".to_string()]);
}

#[test]
fn test_frequency() {
    let input = vec!["a".to_string(), "a".to_string(), "b".to_string()];
    let output = get_freq(input);
    assert_eq!(output.get("a"), Some(&2));
    assert_eq!(output.get("b"), Some(&1));
    assert_eq!(output.get("c"), None);
}

#[test]
fn integration_test() {
    let initial_vec = split_string_into_vec("beefcabbdedcfaccabbe".to_string());
    let data : HashMap<String, i32> = get_freq(initial_vec);
    let output: Vec<String> = sort_to_vec(data);
    show_freq(output);
}

#[test]
fn mostrar_datos_de_arbol() {
    let mut arbol: Node = Node::new("d", 1);
    arbol.put("c", 10);
    arbol.put("f", 4);
    arbol.put("a", 2);
    arbol.put("d", 5);
    arbol.show();
    assert_eq!(arbol.get("d"), Some(1));
    assert_eq!(arbol.get("c"), Some(10));
    assert_eq!(arbol.get("a"), Some(2));
    assert_eq!(arbol.get("f"), Some(4));
    assert_eq!(arbol.get("t"), None);
}
