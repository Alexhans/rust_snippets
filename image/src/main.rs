extern crate image;

use std::fs::File;
use std::path::Path;
use std::env;

use image::GenericImage;

// Modified from https://github.com/PistonDevelopers/image samples
// using std::env to get the proper home dir

// looks for test.png in the home dir and encodes another one with another name
fn main() {
    match env::home_dir() {
        Some(ref p) => {
            // Use the open function to load an image from a Path.
            // ```open``` returns a dynamic image.
            let home_dir = p.display();
            let image_path = home_dir.to_string() + "/test.png";
            println!("opening file from {}", image_path);
            let img = image::open(&Path::new(&image_path)).unwrap();

            // The dimensions method returns the images width and height
            println!("dimensions {:?}", img.dimensions());

            // The color method returns the image's ColorType
            println!("{:?}", img.color());

            let output_image_path = home_dir.to_string() + "/test2.png";
            let ref mut fout = File::create(&Path::new(&output_image_path)).unwrap();

            // Write the contents of this image to the Writer in PNG format.
            let _ = img.save(fout, image::PNG).unwrap();
        }

        None => println!("Impossible to get your home dir, not doing it!")
    }
}
