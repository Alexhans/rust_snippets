use std::thread;

fn main() {
    let result = thread::spawn(move || {
        panic!("oops!");
    }).join();

    assert!(result.is_err());
}
