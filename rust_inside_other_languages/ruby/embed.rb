require 'ffi'

module Hello
    extend FFI::Library
    ffi_lib 'embed/target/release/embed.dll'
    attach_function :process, [], :void
end

Hello.process

puts 'done!'
