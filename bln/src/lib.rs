#![allow(dead_code)]
extern crate image;

use std::fs::File;
use std::io::BufReader;
use std::io::BufRead;
use std::io::Read;
use std::error::Error;

/* use image:: { */
/*     GenericImage, */
/*     ImageBuffer, */
/* }; */

#[derive(Debug)]
#[derive(PartialEq)]
enum ColorCode {
    Amarillo,
    Verde,
    Celeste,
    Rojo,
    Magenta,
    Azul,
    Blanco,
    Naranja,
    Lila,
    AmarilloLimon,
    Rosado,
    CelesteMedio,
    MarronMedio,
    VerdeOscuro,
    RojoOscuro,
}

impl ColorCode {
    fn reflection(&self) -> String {
        format!("{:?}", self).to_string()
    }

    fn binary_value(&self) -> (u8, u8, u8) {
        match *self {
              ColorCode::Amarillo   => (255, 255, 0),
              ColorCode::Verde      => (0, 255, 0),
              ColorCode::Celeste    => (0, 255, 255),
              ColorCode::Rojo       => (255, 0, 0),
              ColorCode::Magenta    => (255, 0, 255),
              ColorCode::Azul       => (0, 0, 255),
              ColorCode::Blanco     => (255, 255, 255),
              ColorCode::Naranja    => (0, 190, 255),
              ColorCode::Lila       => (180, 180, 255),
              ColorCode::AmarilloLimon => (255, 255, 170),
              ColorCode::Rosado     => (200, 180, 200),
              ColorCode::CelesteMedio => (0, 200, 255),
              ColorCode::MarronMedio => (180, 100, 0),
              ColorCode::VerdeOscuro => (0, 130, 0),
              ColorCode::RojoOscuro => (190, 0, 0),        
        }
    }

    fn color_code(&self) -> u8 {
        match *self {
              ColorCode::Amarillo => 01,
              ColorCode::Verde => 02,
              ColorCode::Celeste => 03,
              ColorCode::Rojo => 04,
              ColorCode::Magenta => 05,
              ColorCode::Azul => 06,
              ColorCode::Blanco => 07,
              ColorCode::Naranja => 08,
              ColorCode::Lila => 09,
              ColorCode::AmarilloLimon => 10,
              ColorCode::Rosado => 11,
              ColorCode::CelesteMedio => 12,
              ColorCode::MarronMedio => 13,
              ColorCode::VerdeOscuro => 14,
              ColorCode::RojoOscuro => 15,
        }
    }   
}

struct Bln<'a> {
    lines: i32,
    header: &'a str,
    compressed_lines: Vec<&'a str>,
} 

impl<'a> Bln<'a> {
    fn from_text(input_path: &str) -> Bln {
        let bln_file  = match File::open(input_path) {
            Err(why) => panic!("Couldn't open {}: {}", input_path, Error::description(&why)),
            Ok(file) => file,
        };

        let mut reader = BufReader::new(bln_file);
        let mut buffer_string = String::new();

        let huh = match reader.read_to_string(&mut buffer_string) {
            Err(why) => panic!("HELL NAW"),
            Ok(what) => what,
        };
        // println!("We read a new line: {}", buffer_string);

        let gotten_lines: Option<i32> = buffer_string.lines().nth(0).unwrap().parse::<i32>().ok();
        println!("lines {}", gotten_lines.unwrap());
        println!("header would go here");
        for x in buffer_string.lines().skip(26) {
            println!("{}", x);
        }
        // buffer_string.clear();

        Bln {lines: gotten_lines.unwrap(), header: "", compressed_lines: vec!["1 0 2 5", "2 1 1 6", "4 3 4 5"]} 
    }
}

fn bln_to_image_dummy(input_path: &str) {
    
    // open bln
    /* let bln_file = try!(File::open(input_path)); */
    // read bln
    
    // create an image file
    /* let img = DynamicImage::new(600, 600); */
    // write it with the same name as the bln but different extension
    let output_file = File::create("data_01.png");
    /* img.save("data_01.png").unwrap(); */
    /*     Err(_) => panic!("fuck"), */
    /*     Ok(_) => None, */
    /* }; */
}

#[test]
fn it_works() {
    let color : ColorCode = ColorCode::Celeste;
    // println!("{}", color.reflection());
    // println!("{:?}", color.binary_value());
    // println!("{:?}", color.color_code());
    assert_eq!(color, ColorCode::Celeste);
    assert_eq!(color.binary_value(), (0, 255, 255));
    assert_eq!(color.color_code(), 3);
}

#[test]
fn test_bln_to_image_dummy() {
    let bln_filename : &str = "data_01.bln";    
    bln_to_image_dummy(bln_filename);
    let expected_filename : &str = "data_01.png";
    
    let file = match File::open(expected_filename) {
        Err(_) => panic!("Couldn't open"),
        Ok(file) => file,
    };
}

// ATENTION, possible integration test
#[test] 
fn test_read_bln_file() {
    let bln_filename : &str = "data_01.bln";    
    let expectedBln = Bln {lines: 3, header: "0\n0\n0 0\n0 0\n0 0\n0 0\n0 0\n0 0\n0 0\n0 0 0 0 0\n0 0 0 0 0\n0 0 0 0 0\n0 0 0 0 0\n0 0 0 0 0\n0 0 0 0 0\n0 0 0 0 0\n0 0 0 0 0\n0 0 0 0 0\n0 0 0 0 0\n0 0 0 0 0\n0 0 0 0 0\n0 0 0 0 0\n0 0 0 0 0\n0 0 0 0 0\n0 0 0 0 0\n" ,compressed_lines: vec!["1 0 2 5", "2 1 1 6", "4 3 4 5"] };
    let obtainedBln = Bln::from_text(&bln_filename);
    
    assert_eq!(expectedBln.lines, obtainedBln.lines);
    assert_eq!(expectedBln.compressed_lines, obtainedBln.compressed_lines);
}
